package com.example.disi.mapper;

import com.example.disi.domain.Reservation;
import com.example.disi.domain.SportFacility;
import com.example.disi.domain.UserEntity;
import com.example.disi.dto.user.UserAccountDTO;
import com.example.disi.dto.user.UserEntityDTO;
import com.example.disi.dto.user.UserInsertDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(uses = {AddressMapper.class, SportFacilityMapper.class, ReservationMapper.class})
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserAccountDTO userEntityToUserAccountDTO(UserEntity userEntity);

    UserEntity userAccountDTOToUserEntity(UserAccountDTO userAccountDTO);

    UserEntity userEntityDTOToUserEntity(UserEntityDTO userEntityDTO);

    UserEntity userInsertDTOToUserEntity(UserInsertDTO insertDTO);

    List<UserAccountDTO> userEntityListToUserAccountDTOList(List<UserEntity> users);

    List<UserEntity> userAccountDTOListToUserEntityList(List<UserAccountDTO> users);

}
