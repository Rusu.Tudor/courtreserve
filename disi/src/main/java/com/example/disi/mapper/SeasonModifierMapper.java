package com.example.disi.mapper;

import com.example.disi.domain.SeasonModifier;
import com.example.disi.dto.SeasonModifierDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface SeasonModifierMapper {

    SeasonModifierDTO toDTO(SeasonModifier seasonModifier);
    SeasonModifier fromDTO(SeasonModifierDTO seasonModifierDTO);

    List<SeasonModifierDTO> toDTOList(List<SeasonModifier> seasonModifiers);
    List<SeasonModifier> fromDTOList(List<SeasonModifierDTO> seasonModifierDTOs);
}
