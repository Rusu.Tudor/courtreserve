package com.example.disi.mapper;

import com.example.disi.domain.Address;
import com.example.disi.dto.address.AddressDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AddressMapper {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    AddressDTO addressEntityToAddressDTO(Address address);

    Address addressDTOToAddressEntity(AddressDTO addressDTO);
}
