package com.example.disi.mapper;

import com.example.disi.domain.SportFacility;
import com.example.disi.dto.sportFacility.SportFacilityDTOInput;
import com.example.disi.dto.sportFacility.SportFacilityDTOOutput;
import com.example.disi.dto.sportFacility.SportFacilityDTOUpdate;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(uses = {AddressMapper.class, RateMapper.class, ScheduleMapper.class, TennisCourtMapper.class})
public interface SportFacilityMapper {

    SportFacilityMapper INSTANCE = Mappers.getMapper(SportFacilityMapper.class);

    SportFacilityDTOInput sportFacilityToSportFacilityDTOInput(SportFacility sportFacility);
    SportFacilityDTOOutput sportFacilityToSportFacilityDTOOutput(SportFacility sportFacility);
    SportFacilityDTOUpdate sportFacilityToSportFacilityDTOUpdate(SportFacility sportFacility);
    SportFacility sportFacilityDTOInputToSportFacility(SportFacilityDTOInput sportFacilityDTOInput);
    SportFacility sportFacilityDTOOutputToSportFacility(SportFacilityDTOOutput sportFacilityDTOOutput);
    SportFacility sportFacilityDTOUpdateToSportFacility(SportFacilityDTOUpdate sportFacilityDTOUpdate);
    List<SportFacilityDTOOutput> sportFacilitysToSportFacilityDTOOutputs(List<SportFacility> sportFacilitys);

    @AfterMapping
    public default void mapID(SportFacilityDTOOutput sportFacilityDTOOutput, @MappingTarget SportFacility sportFacility){
        sportFacility.setId(sportFacilityDTOOutput.getId());
    }
}
