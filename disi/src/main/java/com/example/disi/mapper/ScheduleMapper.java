package com.example.disi.mapper;

import com.example.disi.domain.Schedule;
import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOOutput;
import com.example.disi.dto.schedule.ScheduleTimeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {SportFacilityMapper.class})
public interface ScheduleMapper {

    ScheduleMapper INSTANCE = Mappers.getMapper(ScheduleMapper.class);

    ScheduleTimeDTO scheduleToScheduleTimeDTO(Schedule schedule);
    Schedule scheduleDTOInputToSchedule(ScheduleDTOInput scheduleDTOInput);
    Schedule scheduleDTOOutputToSchedule(ScheduleDTOOutput scheduleDTOOutput);
    ScheduleDTOInput scheduleToScheduleDTOInput(Schedule schedule);
    ScheduleDTOOutput scheduleToScheduleDTOOutput(Schedule schedule);
    List<ScheduleDTOOutput> schedulesToScheduleDTOOutputs(List<Schedule> schedules);
}
