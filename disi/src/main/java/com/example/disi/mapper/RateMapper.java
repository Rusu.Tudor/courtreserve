package com.example.disi.mapper;

import com.example.disi.domain.Rate;
import com.example.disi.domain.Reservation;
import com.example.disi.domain.TennisCourt;
import com.example.disi.dto.rate.RateDTOInput;
import com.example.disi.dto.rate.RateDTO;
import com.example.disi.dto.reservation.ReservationDTO;
import com.example.disi.dto.tennisCourt.TennisCourtDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(uses = {HourModifierMapper.class, SeasonModifierMapper.class})
public interface RateMapper {

    RateMapper INSTANCE = Mappers.getMapper(RateMapper.class);
    HourModifierMapper hourModifierMapper = Mappers.getMapper(HourModifierMapper.class);
    SeasonModifierMapper seasonModifierMapper = Mappers.getMapper(SeasonModifierMapper.class);


    RateDTOInput rateToRateDTOInput(Rate rate);
    RateDTO toDTO(Rate rate);
    Rate rateDTOInputToRate(RateDTOInput rateDTOInput);
    Rate fromDTO(RateDTO rateDTOOutput);
    List<RateDTOInput> ratesToRateDTOInputs(List<Rate> rates);
    List<RateDTO> ratesToRateDTOOutputs(List<Rate> rates);

    @AfterMapping
    public default void mapHourModifiers(RateDTO rateDTO, @MappingTarget Rate rate) {
        rate.setHourModifier(hourModifierMapper.fromDTOList(rateDTO.getHourModifiers()));
        rate.setSeasonModifier(seasonModifierMapper.fromDTOList(rateDTO.getSeasonModifiers()));
    }

    @AfterMapping
    public default void mapHourModifiers(Rate rate, @MappingTarget RateDTO rateDTO) {
        rateDTO.setHourModifiers(hourModifierMapper.toDTOList(rate.getHourModifier()));
        rateDTO.setSeasonModifiers(seasonModifierMapper.toDTOList(rate.getSeasonModifier()));
    }

    @AfterMapping
    public default void mapID(RateDTOInput rateDTOInput, @MappingTarget Rate rate){
        rate.setId(rateDTOInput.getId());
    }
}
