package com.example.disi.mapper;

import com.example.disi.domain.Reservation;
import com.example.disi.domain.SportFacility;
import com.example.disi.domain.Subscription;
import com.example.disi.domain.TennisCourt;
import com.example.disi.dto.subscription.SubscriptionDTO;
import com.example.disi.dto.tennisCourt.TennisCourtDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {ReservationMapper.class, SubscriptionMapper.class})
public interface TennisCourtMapper {
    TennisCourtMapper INSTANCE = Mappers.getMapper(TennisCourtMapper.class);

    TennisCourtDTO tennisCourtToTennisCourtDTO(TennisCourt tennisCourt);
    TennisCourt tennisCourtDTOToTennisCourt(TennisCourtDTO tennisCourtDTO);

    List<TennisCourtDTO> tennisCourtsToTennisCourtsDTO(List<TennisCourt> tennisCourts);

    @AfterMapping
    public default void mapID(TennisCourtDTO tennisCourtDTO, @MappingTarget TennisCourt tennisCourt){
        tennisCourt.setId(tennisCourtDTO.getId());
    }
}
