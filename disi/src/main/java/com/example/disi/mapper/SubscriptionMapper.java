package com.example.disi.mapper;

import com.example.disi.domain.Subscription;
import com.example.disi.dto.subscription.SubscriptionDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SubscriptionMapper {

    SubscriptionMapper INSTANCE = Mappers.getMapper(SubscriptionMapper.class);

    SubscriptionDTO subscriptionToSubscriptionDTO(Subscription subscription);
    Subscription subscriptionDTOToSubscription(SubscriptionDTO subscriptionDTO);

    @AfterMapping
    public default void mapID(SubscriptionDTO subscriptionDTO, @MappingTarget Subscription subscription){
        subscription.setId(subscriptionDTO.getId());
    }
}
