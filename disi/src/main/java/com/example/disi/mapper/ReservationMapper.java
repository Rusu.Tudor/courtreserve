package com.example.disi.mapper;

import com.example.disi.domain.Reservation;
import com.example.disi.dto.reservation.ReservationDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ReservationMapper {
    ReservationMapper INSTANCE = Mappers.getMapper(ReservationMapper.class);

    ReservationDTO reservationToReservationDTO(Reservation reservation);
    Reservation reservationDTOtoReservation(ReservationDTO reservationDTO);

    @AfterMapping
    public default void mapID(ReservationDTO reservationDTO, @MappingTarget Reservation reservation){
        reservation.setId(reservationDTO.getId());
    }
}
