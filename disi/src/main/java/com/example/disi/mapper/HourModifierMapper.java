package com.example.disi.mapper;

import com.example.disi.domain.HourModifier;
import com.example.disi.domain.SeasonModifier;
import com.example.disi.dto.HourModifierDTO;
import com.example.disi.dto.SeasonModifierDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface HourModifierMapper {

    HourModifierDTO toDTO(HourModifier hourModifier);
    HourModifier fromDTO(HourModifierDTO hourModifierDTO);

    List<HourModifierDTO> toDTOList(List<HourModifier> hourModifiers);
    List<HourModifier> fromDTOList(List<HourModifierDTO> hourModifierDTOs);
}
