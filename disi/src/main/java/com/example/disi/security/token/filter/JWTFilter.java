package com.example.disi.security.token.filter;

import com.example.disi.domain.UserEntity;
import com.example.disi.security.token.utility.TokenUtility;
import com.example.disi.service.user.UserServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
public class JWTFilter extends OncePerRequestFilter {


    @Autowired
    private UserServiceImpl UserServiceImpl;

    @Autowired
    private TokenUtility tokenUtility;

    private Claims getAllClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey("cheie")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }


    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username != null && username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public boolean isTokenExpired(String token) {
        Date expireDate = getAllClaimsFromToken(token).getExpiration();
        return expireDate.before(new Date());
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String tokenGotFromRequest = tokenUtility.getToken(httpServletRequest);// luam tokenul, taiem 'Bearer ' string care sta la inceputul tokenului.
        if (tokenGotFromRequest != null) {
            String username = getUsernameFromToken(tokenGotFromRequest); // luam username
            if (username != null) { // il validam
                UserEntity currentUser = (UserEntity) UserServiceImpl.loadUserByUsername(username); // luam userul by usenrame
                if(validateToken(tokenGotFromRequest, currentUser)){ // validam tokenul, nu e exiprat, etc.
                    UsernamePasswordAuthenticationToken userPasswordtoken = new UsernamePasswordAuthenticationToken(currentUser,null,currentUser.getAuthorities());
                    //default token, spring security foloseste asta pentru autentificare.
                    userPasswordtoken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    //setam detaliile, ii rpactic ceea ce se intampla pe default, dar avem if-urile, care practic ne limiteaza accesul gen
                    SecurityContextHolder.getContext().setAuthentication(userPasswordtoken);
                    //httpServletResponse.addHeader("Access-Control-Expose-Headers","Authorization");
                    //setam contextul, tot asa ar face spring security default.
                }
            }
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);//continua filtrarea pe request, le lasam controlul la urmatorul filtru practic.
    }
}
