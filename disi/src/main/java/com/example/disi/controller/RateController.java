package com.example.disi.controller;


import com.example.disi.dto.rate.RateDTOInput;
import com.example.disi.dto.rate.RateDTO;
import com.example.disi.service.rate.RateService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/rates")
public class RateController {

    private final RateService rateService;

    public RateController(RateService rateService) {
        this.rateService = rateService;
    }

    @GetMapping
    public List<RateDTO> getAll(){
        return rateService.getAll();
    }

    @GetMapping("/{id}")
    public RateDTO getById(@PathVariable("id") Long id){
        return rateService.getById(id);
    }

    @PostMapping
    public RateDTOInput create(@RequestBody RateDTOInput rateDTOInput){
        return rateService.create(rateDTOInput);
    }

    @PutMapping("/{id}")
    public RateDTOInput update(@PathVariable("id") Long id, @RequestBody RateDTOInput rateDTOInput){
        return rateService.update(id, rateDTOInput);
    }

    @DeleteMapping("/{id}")
    public void delete (@PathVariable("id") Long id){
        rateService.delete(id);
    }
}
