package com.example.disi.controller;

import com.example.disi.domain.Schedule;
import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOOutput;
import com.example.disi.service.schedule.ScheduleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/schedules")
public class ScheduleController {

    private final ScheduleService scheduleService;

    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @GetMapping
    public List<ScheduleDTOOutput> getAll(){
        return scheduleService.getAll();
    }

    @GetMapping("/{id}")
    public ScheduleDTOOutput getById(@PathVariable("id") Long id){
        return scheduleService.getById(id);
    }

    @GetMapping("/sportFacility/{sportFacilityId}")
    public List<ScheduleDTOOutput> getAllBySportFacility(@PathVariable("sportFacilityId") Long sportFacilityId){
        return scheduleService.getAllBySportFacility(sportFacilityId);
    }

    @PostMapping("/sportFacility/{sportFacilityId}")
    public ScheduleDTOInput create(@PathVariable("sportFacilityId") Long sportFacilityId, @RequestBody ScheduleDTOInput scheduleDTOInput){
        return scheduleService.create(sportFacilityId, scheduleDTOInput);
    }

    @PutMapping("/{id}")
    public ScheduleDTOInput update(@PathVariable Long id, @RequestBody ScheduleDTOInput scheduleDTOInput){
        return scheduleService.update(id, scheduleDTOInput);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        scheduleService.delete(id);
    }
}
