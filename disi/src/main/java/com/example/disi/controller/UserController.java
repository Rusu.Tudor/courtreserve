package com.example.disi.controller;

import com.example.disi.domain.RoleEntity;
import com.example.disi.domain.UserEntity;
import com.example.disi.dto.user.UserAccountDTO;
import com.example.disi.dto.user.UserEntityDTO;
import com.example.disi.dto.user.UserInsertDTO;
import com.example.disi.service.role.RoleService;
import com.example.disi.service.user.UserService;
import com.example.disi.service.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping(value = "/users")
public class UserController {
    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public UserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Long> insertUser(@Valid @RequestBody UserInsertDTO userInsertDTO) {
        RoleEntity clientRole = roleService.getRoleByName("ROLE_CLIENT");
        Long userID = userService.insert(userInsertDTO, clientRole);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserAccountDTO> getListOfUsers() {
        return userService.getAllUsers();
    }


    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UserEntityDTO updateUser(@PathVariable Long id, @RequestBody UserEntityDTO userEntityDTO) {
        return userService.updateUser(id, userEntityDTO);
    }

    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UserAccountDTO getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }


}
