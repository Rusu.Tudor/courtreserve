package com.example.disi.controller;

import com.example.disi.domain.Reservation;
import com.example.disi.domain.Subscription;
import com.example.disi.dto.reservation.ReservationDTO;
import com.example.disi.dto.subscription.SubscriptionDTO;
import com.example.disi.service.subscription.SubscriptionServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/subscription")
public class SubscriptionController {
    private final SubscriptionServiceImplementation subscriptionServiceImplementation;

    @Autowired
    public SubscriptionController(SubscriptionServiceImplementation subscriptionServiceImplementation) {
        this.subscriptionServiceImplementation = subscriptionServiceImplementation;
    }

    @PostMapping(value = "/create/{id}")
    public ResponseEntity<Long> insertSubscription(@PathVariable Long id, @Valid @RequestBody SubscriptionDTO subscriptionDTO) {
        Long subscriptionID = subscriptionServiceImplementation.insert(subscriptionDTO, id);
        return new ResponseEntity<>(subscriptionID, HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<List<Subscription>> getSubscriptions(){
        return new ResponseEntity<>(subscriptionServiceImplementation.findAll(), HttpStatus.OK);
    }
    @DeleteMapping("delete/{id}")
    public ResponseEntity<Long> deleteSubscription(@PathVariable Long id){
        subscriptionServiceImplementation.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Subscription> getSubscription(@PathVariable Long id){
        return new ResponseEntity<>(subscriptionServiceImplementation.findByID(id), HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Long> updateSubscription(@Valid @RequestBody SubscriptionDTO subscriptionDTO) {
        Long reservationID = subscriptionServiceImplementation.update(subscriptionDTO);
        return new ResponseEntity<>(reservationID, HttpStatus.OK);
    }
}
