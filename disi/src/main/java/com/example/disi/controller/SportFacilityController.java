package com.example.disi.controller;

import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOOutput;
import com.example.disi.dto.sportFacility.SportFacilityDTOInput;
import com.example.disi.dto.sportFacility.SportFacilityDTOOutput;
import com.example.disi.dto.sportFacility.SportFacilityDTOUpdate;
import com.example.disi.service.sportFacility.SportFacilityService;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/sportFacilities")
public class SportFacilityController {

    private final SportFacilityService sportFacilityService;

    public SportFacilityController(SportFacilityService sportFacilityService) {
        this.sportFacilityService = sportFacilityService;
    }

    @GetMapping
    public List<SportFacilityDTOOutput> getAll(){
        return sportFacilityService.getAll();
    }

    @GetMapping("/{id}")
    public SportFacilityDTOOutput getById(@PathVariable("id") Long id){
        return sportFacilityService.getById(id);
    }

    @GetMapping("/address/{addressId}")
    public SportFacilityDTOOutput getByAddress(@PathVariable("addressId") Long addressId){
        return sportFacilityService.getByAddress(addressId);
    }

    @GetMapping("/rate/{rateId}")
    public SportFacilityDTOOutput getByRate(@PathVariable("rateId") Long rateId){
        return sportFacilityService.getByRate(rateId);
    }

    @GetMapping("/user/{userId}")
    public List<SportFacilityDTOOutput> getAllByUser(@PathVariable("userId") Long userId){
        return sportFacilityService.getAllByUser(userId);
    }

    @PostMapping("/rate/{rateId}")
    public SportFacilityDTOInput create(@PathVariable("rateId") Long rateId, @RequestBody SportFacilityDTOInput sportFacilityDTOInput){
        return sportFacilityService.create(rateId, sportFacilityDTOInput);
    }

    @PutMapping("/{id}")
    public SportFacilityDTOUpdate update(@PathVariable("id") Long id, @RequestBody SportFacilityDTOUpdate sportFacilityDTOUpdate){
        return sportFacilityService.update(id, sportFacilityDTOUpdate);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id){
        sportFacilityService.delete(id);
    }

}
