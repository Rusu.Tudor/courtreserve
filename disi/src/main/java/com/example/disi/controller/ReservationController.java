package com.example.disi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.disi.domain.Reservation;
import com.example.disi.dto.reservation.ReservationDTO;
import com.example.disi.service.reservation.ReservationServiceImplementation;
import com.example.disi.service.reservation.WebSocketFindPlayer;
import com.example.disi.service.tennisCourt.TennisCourtServiceImplementation;
import com.example.disi.service.user.UserServiceImpl;

@CrossOrigin
@RestController
@RequestMapping(value = "/reservation")
public class ReservationController {

    private final UserServiceImpl userService;
    private final ReservationServiceImplementation reservationService;
    private final TennisCourtServiceImplementation tennisCourtServiceImplementation;
    private final WebSocketFindPlayer webSocketFindPlayer;

    @Autowired
    public ReservationController(UserServiceImpl userService, ReservationServiceImplementation reservationService, TennisCourtServiceImplementation tennisCourtServiceImplementation, WebSocketFindPlayer webSocketFindPlayer) {
        this.userService = userService;
        this.reservationService = reservationService;
        this.tennisCourtServiceImplementation = tennisCourtServiceImplementation;
        this.webSocketFindPlayer = webSocketFindPlayer;
    }

    @PostMapping(value = "/create/{id}")
    public ResponseEntity<Long> insertReservation(@PathVariable Long id, @Valid @RequestBody ReservationDTO reservationDTO) {
        Long reservationID = reservationService.insert(reservationDTO, id);
        return new ResponseEntity<>(reservationID, HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<List<Reservation>> getReservations() {
        return new ResponseEntity<>(reservationService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Long> deleteReservation(@PathVariable Long id) {
        return new ResponseEntity<>(reservationService.delete(id), HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Reservation> getReservations(@PathVariable Long id) {
        return new ResponseEntity<>(reservationService.findByID(id), HttpStatus.OK);
    }

    @PostMapping(value = "/update")
    public ResponseEntity<Long> updateReservation(@Valid @RequestBody ReservationDTO reservationDTO) {
        Long reservationID = reservationService.update(reservationDTO);
        return new ResponseEntity<>(reservationID, HttpStatus.OK);
    }

    @PostMapping(value = "/findPlayer/{reservationId}")
    public ResponseEntity<Object> findPlayer(@PathVariable("reservationId") Long reservationId) {
        String message = "Another player is trying to find someone to play with.";
        Reservation reservation = reservationService.findByID(reservationId);
        if(!reservation.getAccepted() && !reservation.getReservationTakeoverRequest()) {
            reservation.setPlayerRequest(true);
            Long id = reservationService.setFlagsReservation(reservation);
            System.out.println(id);
            webSocketFindPlayer.sendNotificationToPlayers(message);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/acceptPlayer/{userId}/reservation/{reservationId}")
    public ResponseEntity<Object> acceptPlayer(@PathVariable("userId") Long userId, @PathVariable("reservationId") Long reservationId) {
        reservationService.acceptPlayer(userId, reservationId);
        return new ResponseEntity<>(HttpStatus.OK);

    }


    @PostMapping(value = "/replaceReservation/{reservationId}")
    public ResponseEntity<Object> replaceReservation(@PathVariable("reservationId") Long reservationId) {
        String message = "Another player is trying to replace their reservation.";
        Reservation reservation = reservationService.findByID(reservationId);
        if(!reservation.getAccepted() && !reservation.getPlayerRequest()) {
            reservation.setReservationTakeoverRequest(true);
            Long id = reservationService.setFlagsReservation(reservation);
            System.out.println(id);
            webSocketFindPlayer.sendNotificationToReplaceReservation(message);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/acceptReplaceReservation/{userId}/reservation/{reservationId}")
    public ResponseEntity<Object> acceptReplaceReservation(@PathVariable("userId") Long userId, @PathVariable("reservationId") Long reservationId) {
        reservationService.acceptReplaceReservation(userId, reservationId);
        return new ResponseEntity<>(HttpStatus.OK);

    }
    
    @GetMapping("/user/{id}")
    public List<Reservation> getReservationsByUser(@PathVariable("id") final Long userId) {
    	return reservationService.getAllByUser(userId);
    }
}
