package com.example.disi.controller;

import com.example.disi.domain.Subscription;
import com.example.disi.domain.TennisCourt;
import com.example.disi.dto.reservation.AddReservationDTO;
import com.example.disi.dto.reservation.ReservationDTO;
import com.example.disi.dto.subscription.AddSubscriptionDTO;
import com.example.disi.dto.subscription.SubscriptionDTO;
import com.example.disi.dto.tennisCourt.TennisCourtDTO;
import com.example.disi.service.tennisCourt.TennisCourtServiceImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/tennisCourt")
public class TennisCourtController {

    private final TennisCourtServiceImplementation tennisCourtServiceImplementation;

    @Autowired
    public TennisCourtController(TennisCourtServiceImplementation tennisCourtServiceImplementation) {
        this.tennisCourtServiceImplementation = tennisCourtServiceImplementation;
    }

    @PostMapping(value = "/create/{sportFacilityId}")
    public ResponseEntity<Long> insertTennisCourt(@Valid @RequestBody TennisCourtDTO tennisCourtDTO, @PathVariable Long sportFacilityId) {
        Long tennisCourtID = tennisCourtServiceImplementation.insert(tennisCourtDTO, sportFacilityId);
        return new ResponseEntity<>(tennisCourtID, HttpStatus.OK);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<List<TennisCourtDTO>> getTennisCourts(){
        return new ResponseEntity<>(tennisCourtServiceImplementation.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteTennisCourt(@PathVariable final Long id) {
    	tennisCourtServiceImplementation.delete(id);
    }
    
    @GetMapping("/sportFacility/{id}")
    public List<TennisCourtDTO> getAllTennisCourtBySportFacility(@PathVariable("id") final Long sportFacilityId) {
		return tennisCourtServiceImplementation.getAllBySportFacility(sportFacilityId);
    }
}
