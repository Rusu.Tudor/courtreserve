package com.example.disi.controller;

import com.example.disi.domain.UserEntity;
import com.example.disi.dto.jwt.JWTDao;
import com.example.disi.dto.user.UserAccountDTO;
import com.example.disi.security.token.utility.TokenUtility;
import com.example.disi.service.role.RoleService;
import com.example.disi.service.user.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/")
public class IndexController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    private final UserServiceImpl UserServiceImpl;

    @Autowired
    private final RoleService roleService;

    @Autowired
    private AuthenticationManager authentificationManager;

    @Autowired
    private TokenUtility tokenUtility;

    @Autowired
    public IndexController(UserServiceImpl UserServiceImpl, RoleService roleService) {
        this.UserServiceImpl = UserServiceImpl;
        this.roleService = roleService;
    }

    @GetMapping()
    public ResponseEntity<Object> index() {
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping()
    public ResponseEntity<?> loginUser(@Valid @RequestBody UserAccountDTO userAccountDTO) {

        final Authentication authenticate = authentificationManager.authenticate(new UsernamePasswordAuthenticationToken(userAccountDTO.getEmail(), userAccountDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        UserEntity user = (UserEntity) authenticate.getPrincipal();
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();
        return ResponseEntity.ok(new JWTDao(user.getId(), user.getRoleEntity().getName(), user.getUsername(), tokenUtility.generateToken(user.getUsername())));
    }
}
