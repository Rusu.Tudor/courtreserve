package com.example.disi.repository;

import com.example.disi.domain.Address;
import com.example.disi.domain.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
