package com.example.disi.repository;

import com.example.disi.domain.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    RoleEntity findFirstByName(String name);
}
