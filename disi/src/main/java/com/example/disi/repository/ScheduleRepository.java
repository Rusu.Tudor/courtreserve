package com.example.disi.repository;

import com.example.disi.domain.Schedule;

import com.example.disi.domain.SportFacility;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    List<Schedule> findAllBySportFacility(SportFacility sportFacility);
}
