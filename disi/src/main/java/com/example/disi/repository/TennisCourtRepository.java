package com.example.disi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.disi.domain.SportFacility;
import com.example.disi.domain.TennisCourt;

public interface TennisCourtRepository extends JpaRepository<TennisCourt, Long> {

	List<TennisCourt> findAllBySportFacility(SportFacility sportFacility);
}
