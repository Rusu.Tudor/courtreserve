package com.example.disi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.disi.domain.Reservation;
import com.example.disi.domain.UserEntity;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findByTennisCourtId(Long id);
    List<Reservation> findAllByUserEntity(UserEntity userEntity);
}
