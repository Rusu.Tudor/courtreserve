package com.example.disi.repository;

import com.example.disi.domain.HourModifier;
import com.example.disi.domain.Rate;
import com.example.disi.domain.Reservation;
import com.example.disi.domain.SeasonModifier;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface HourModifierRepository extends JpaRepository<HourModifier, Long> {

    Optional<HourModifier> findFirstByRateAndDayOfTheWeek(Rate rate, DayOfTheWeek dayOfTheWeek);
}
