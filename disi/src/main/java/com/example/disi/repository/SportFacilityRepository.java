package com.example.disi.repository;

import com.example.disi.domain.Address;
import com.example.disi.domain.Rate;
import com.example.disi.domain.SportFacility;
import com.example.disi.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SportFacilityRepository extends JpaRepository<SportFacility, Long> {
    Optional<SportFacility> findByAddress(Address address);
    Optional<SportFacility> findByRate(Rate rate);
    List<SportFacility> findAllByUserEntity(UserEntity userEntity);
}
