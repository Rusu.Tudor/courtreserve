package com.example.disi.domain;

import com.example.disi.domain.commons.BaseEntity;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Rate extends BaseEntity {

    @Column(name = "nocturneModifier")
    private Float nocturneModifier;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, targetEntity = SeasonModifier.class, mappedBy = "rate")
    private List<SeasonModifier> seasonModifier;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, targetEntity = HourModifier.class, mappedBy = "rate")
    private List<HourModifier> hourModifier;
}
