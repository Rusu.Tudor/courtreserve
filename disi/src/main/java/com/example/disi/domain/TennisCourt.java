package com.example.disi.domain;

import com.example.disi.domain.commons.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TennisCourt extends BaseEntity {

    @Column(name = "type")
    private String type;

    @Column(name = "nocturne")
    private Boolean nocturne;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SportFacility sportFacility;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "tennis_court_id")
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Reservation.class)
    private List<Reservation> reservations;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "tennis_court_id")
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Subscription.class)
    private List<Subscription> subscriptions;
}
