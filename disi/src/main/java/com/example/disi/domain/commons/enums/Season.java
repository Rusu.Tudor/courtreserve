package com.example.disi.domain.commons.enums;

import java.time.Month;

public enum Season {
    SUMMER, SPRING, WINTER, AUTUMN;

    static public Season getSeasonByMonth (Month month){
        switch (month){
            case JANUARY: return Season.WINTER;
            case FEBRUARY: return Season.WINTER;
            case MARCH: return Season.SPRING;
            case APRIL: return Season.SPRING;
            case MAY: return Season.SPRING;
            case JUNE: return Season.SUMMER;
            case JULY: return Season.SUMMER;
            case AUGUST: return Season.SUMMER;
            case SEPTEMBER: return Season.AUTUMN;
            case OCTOBER: return Season.AUTUMN;
            case NOVEMBER: return Season.AUTUMN;
            case DECEMBER: return Season.WINTER;
            default: return null;
        }
    }
}
