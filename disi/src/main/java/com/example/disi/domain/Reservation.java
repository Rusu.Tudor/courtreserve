package com.example.disi.domain;

import com.example.disi.domain.commons.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Reservation extends BaseEntity {
    @Column(name = "time")
    private LocalDateTime time;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "month")
    private Integer month;

    @Column(columnDefinition = "boolean default false")
    private Boolean accepted;

    @Column(columnDefinition = "boolean default false")
    private Boolean playerRequest;

    @Column(columnDefinition = "boolean default false")
    private Boolean reservationTakeoverRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.TRUE)
    @JsonIgnore()
    private TennisCourt tennisCourt;

    @ManyToOne(fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.TRUE)
    @JsonIgnore()
    private UserEntity userEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.TRUE)
    @JsonIgnore()
    private UserEntity secondUserEntity;

}
