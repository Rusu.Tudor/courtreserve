package com.example.disi.domain;


import com.example.disi.domain.commons.BaseEntity;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Schedule extends BaseEntity {

    @Min(0)
    @Max(23)
    @Column(name = "openingTime")
    private Integer openingTime;

    @Min(0)
    @Max(23)
    @Column(name = "closingTime")
    private Integer closingTime;

    @Enumerated(EnumType.STRING)
    private Season season;

    @Enumerated(EnumType.STRING)
    private DayOfTheWeek dayOfTheWeek;

    @ManyToOne
    @JoinColumn(name = "sportFacility", referencedColumnName = "id")
    private SportFacility sportFacility;


}
