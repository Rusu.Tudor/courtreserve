package com.example.disi.domain;

import com.example.disi.domain.commons.BaseEntity;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@Entity
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class RoleEntity extends BaseEntity implements Serializable, GrantedAuthority {

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(targetEntity = UserEntity.class, mappedBy = "roleEntity",fetch = FetchType.EAGER)
    private List<UserEntity> userEntity;

    @Override
    public String getAuthority() {
        return name;
    }
}
