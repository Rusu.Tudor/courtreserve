package com.example.disi.domain;

import com.example.disi.domain.commons.BaseModifier;
import com.example.disi.domain.commons.enums.Season;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Data
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class SeasonModifier extends BaseModifier {

    @Enumerated(EnumType.STRING)
    private Season season;
}
