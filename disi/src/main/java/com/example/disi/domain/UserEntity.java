package com.example.disi.domain;

import com.example.disi.domain.commons.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.sql.Date;
import java.util.List;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


@Entity
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity extends BaseEntity implements Serializable, UserDetails  {

    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "firstName", nullable = false)
    private String firstName;

    @Column(name = "lastName", nullable = false)
    private String lastName;

    @Column(name = "birthDate")
    private Date birthDate;

    @OneToOne()
    private Address address;

    @ManyToOne()
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private RoleEntity roleEntity;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "user_entity_id")
    @OneToMany(cascade = CascadeType.ALL, targetEntity = SportFacility.class)
    private List<SportFacility> sportFacilities;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "user_entity_id")
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Reservation.class)
    private List<Reservation> reservations;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinColumn(name = "user_entity_id")
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Subscription.class)
    private List<Subscription> subscriptions;



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<RoleEntity> roleEntityList = new ArrayList<>();
        roleEntityList.add(roleEntity);
        return roleEntityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
