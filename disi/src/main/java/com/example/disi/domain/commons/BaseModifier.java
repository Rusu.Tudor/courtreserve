package com.example.disi.domain.commons;

import com.example.disi.domain.Rate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Data
@ToString
@EqualsAndHashCode
@MappedSuperclass
public class BaseModifier extends BaseEntity {

    @Column(name = "modifier")
    private float modifier;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "rate", referencedColumnName = "id")
    private Rate rate;
}
