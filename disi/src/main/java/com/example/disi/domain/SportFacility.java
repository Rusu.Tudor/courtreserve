package com.example.disi.domain;

import com.example.disi.domain.commons.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SportFacility extends BaseEntity {
    @Column(name = "basePrice")
    private Float basePrice;

    @JoinColumn(name = "addressID")
    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    @JoinColumn(name = "rateID")
    @OneToOne(cascade = CascadeType.ALL)
    private Rate rate;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Schedule.class, orphanRemoval = true, mappedBy = "sportFacility")
    private List<Schedule> schedules;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL, targetEntity = TennisCourt.class)
    private List<TennisCourt> tennisCourts;

    @ManyToOne(fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.TRUE)
    @JsonIgnore()
    private UserEntity userEntity;

}
