package com.example.disi.domain.commons.enums;

import java.time.DayOfWeek;

public enum DayOfTheWeek {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;

    static public DayOfTheWeek getDayOfTheWeekByDayOfWeek (DayOfWeek dayOfWeek){
        switch (dayOfWeek){
            case MONDAY: return DayOfTheWeek.MONDAY;
            case TUESDAY: return DayOfTheWeek.TUESDAY;
            case WEDNESDAY: return DayOfTheWeek.WEDNESDAY;
            case THURSDAY: return DayOfTheWeek.THURSDAY;
            case FRIDAY: return DayOfTheWeek.FRIDAY;
            case SATURDAY: return DayOfTheWeek.SATURDAY;
            case SUNDAY: return DayOfTheWeek.SUNDAY;
            default: return null;
        }
    }
}
