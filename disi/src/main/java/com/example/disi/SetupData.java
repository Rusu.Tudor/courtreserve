package com.example.disi;

import com.example.disi.domain.Address;
import com.example.disi.domain.RoleEntity;
import com.example.disi.domain.UserEntity;
import com.example.disi.repository.AddressRepository;
import com.example.disi.repository.RoleRepository;
import com.example.disi.repository.UserRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.util.UUID;

@Component
public class SetupData implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadyDone = false;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Transactional
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (alreadyDone)
            return;
        if (roleRepository.findFirstByName("ROLE_ADMIN") == null) {
            RoleEntity clientRole = new RoleEntity();
            clientRole.setName("ROLE_CLIENT");
            clientRole.setUuid(UUID.randomUUID());
            roleRepository.save(clientRole);
        }

        if (!userRepository.findFirstByEmail("admin").isPresent()) {
            RoleEntity adminRole = new RoleEntity();
            adminRole.setName("ROLE_ADMIN");
            adminRole.setUuid(UUID.randomUUID());
            roleRepository.save(adminRole);

            Address address = new Address("Cluj","Dorobantilor","3","Romania");
            addressRepository.save(address);

            UserEntity adminUser = new UserEntity();
            adminUser.setEmail("admin@disi.com");
            adminUser.setFirstName("admin");
            adminUser.setLastName("admin");
            adminUser.setPassword(passwordEncoder.encode("admin"));
            adminUser.setAddress(address);
            adminUser.setBirthDate(new java.sql.Date(new java.util.Date().getTime()));
            adminUser.setRoleEntity(adminRole);
            adminUser.setUuid(UUID.randomUUID());
            userRepository.save(adminUser);
        }
        alreadyDone = true;
    }
}
