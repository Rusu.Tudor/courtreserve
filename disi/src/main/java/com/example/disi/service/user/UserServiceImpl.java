	package com.example.disi.service.user;

import com.example.disi.domain.Address;
import com.example.disi.domain.RoleEntity;
import com.example.disi.domain.UserEntity;
import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.address.AddressDTO;
import com.example.disi.dto.user.UserAccountDTO;
import com.example.disi.dto.user.UserEntityDTO;
import com.example.disi.dto.user.UserInsertDTO;
import com.example.disi.mapper.AddressMapper;
import com.example.disi.mapper.UserMapper;
import com.example.disi.repository.AddressRepository;
import com.example.disi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    private static final String ENTITY = "The entity was not found";
    private static final String IDENTITY = "There was an error in the specified task with id {}";

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;

    private PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AddressRepository addressRepository) {
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s){
        Optional<UserEntity> userEntity = userRepository.findFirstByEmail(s);
        return userEntity.orElse(null);
    }

    public Long insert(UserInsertDTO userAccountDTO, RoleEntity roleEntity) {
        if(userRepository.findFirstByEmail(userAccountDTO.getEmail()).isPresent()){
            throw new ResourceNotFoundException(UserEntity.class.getSimpleName() + "not found");
        }

        UserEntity user = UserMapper.INSTANCE.userInsertDTOToUserEntity(userAccountDTO);
        user.setRoleEntity(roleEntity);
        AddressDTO addressDTO = userAccountDTO.getAddress();
        Address address = AddressMapper.INSTANCE.addressDTOToAddressEntity(addressDTO);
        user.setAddress(address);
        user.setUuid(UUID.randomUUID());
        addressRepository.save(address);
        userRepository.save(user);
        LOGGER.debug("User with id {} was inserted in db", user.getId());
        return user.getId();
    }

    @Override
    public List<UserAccountDTO> getAllUsers() {
        LOGGER.debug("Getting all users...");
        List<UserEntity> userEntityList = (List<UserEntity>) userRepository.findAll();
        return UserMapper.INSTANCE.userEntityListToUserAccountDTOList(userEntityList);
    }

    @Override
    public UserAccountDTO getUserById(Long id) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(id);
        if (!optionalUserEntity.isPresent()) {
            LOGGER.error("There was an error in the specified user with id {}", id);
            throw new ResourceNotFoundException(ENTITY);
        } else {
            UserEntity userEntity = optionalUserEntity.get();
            return UserMapper.INSTANCE.userEntityToUserAccountDTO(userEntity);
        }
    }

    @Override
    public UserEntityDTO updateUser(Long id, UserEntityDTO userAccountDTO) {
        LOGGER.debug("Updating a user with uuid {}", id);
        UserEntity userEntity = UserMapper.INSTANCE.userEntityDTOToUserEntity(userAccountDTO);
        Optional<UserEntity> userById = userRepository.findById(id);
        if (!userById.isPresent()) {
            LOGGER.error(IDENTITY, id);
            throw new ResourceNotFoundException(ENTITY);
        } else {
            userEntity.setId(userById.get().getId());
            userEntity.setRoleEntity(userById.get().getRoleEntity());
            userEntity.setAddress(userById.get().getAddress());
            userRepository.save(userEntity);
            return userAccountDTO;
        }
    }

    @Override
    public void deleteUser(Long id) {
        Optional<UserEntity> userById = userRepository.findById(id);
        if (!userById.isPresent()) {
            LOGGER.error(IDENTITY, id);
            throw new ResourceNotFoundException(ENTITY);
        } else {
            LOGGER.debug("Deleting a user with uuid {}", id);
            userRepository.deleteById(id);
        }
    }

}
