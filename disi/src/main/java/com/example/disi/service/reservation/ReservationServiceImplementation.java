package com.example.disi.service.reservation;

import com.example.disi.domain.*;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import com.example.disi.dto.email.EmailDTO;
import com.example.disi.dto.reservation.ReservationDTO;
import com.example.disi.mapper.ReservationMapper;
import com.example.disi.repository.*;
import com.example.disi.service.email.EmailService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import freemarker.template.TemplateException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class ReservationServiceImplementation implements ReservationService{
    private final ReservationRepository reservationRepository;
    private final TennisCourtRepository tennisCourtRepository;
    private final UserRepository userRepository;
    private final SportFacilityRepository sportFacilityRepository;
    private final RateRepository rateRepository;
    private final SeasonModifierRepository seasonModifierRepository;
    private final HourModifierRepository hourModifierRepository;
    private final EmailService emailService;

    public ReservationServiceImplementation(ReservationRepository reservationRepository, TennisCourtRepository tennisCourtRepository, UserRepository userRepository, SportFacilityRepository sportFacilityRepository, RateRepository rateRepository, SeasonModifierRepository seasonModifierRepository, HourModifierRepository hourModifierRepository, EmailService emailService) {
        this.reservationRepository = reservationRepository;
        this.tennisCourtRepository = tennisCourtRepository;
        this.userRepository = userRepository;
        this.sportFacilityRepository = sportFacilityRepository;
        this.rateRepository = rateRepository;
        this.seasonModifierRepository = seasonModifierRepository;
        this.hourModifierRepository = hourModifierRepository;
        this.emailService = emailService;
    }

    public Long insert(ReservationDTO reservationDTO, Long id){
        Reservation reservation = ReservationMapper.INSTANCE.reservationDTOtoReservation(reservationDTO);
        TennisCourt tennisCourt = tennisCourtRepository.getById(id);

        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();

        UserEntity user = userRepository.findFirstByEmail(username).get();

        reservation.setTennisCourt(tennisCourt);
        reservation.setUserEntity(user);
        reservation.setAccepted(false);
        reservation.setPlayerRequest(false);
        reservation.setReservationTakeoverRequest(false);

        //get all reservation to check date

        List<Reservation> reservations = reservationRepository.findByTennisCourtId(id);

        int okay = 1;
        for(Reservation res: reservations){
            if(     res.getTime().getYear() == reservation.getTime().getYear() &&
                    res.getTime().getMonth() == reservation.getTime().getMonth() &&
                    res.getTime().getDayOfMonth() == reservation.getTime().getDayOfMonth()
            ){
                // new res + duration < hour start sau new res start > start res exist
                if( !(reservation.getTime().getHour() + reservation.getDuration() <= res.getTime().getHour() ||
                        reservation.getTime().getHour() >= res.getTime().getHour() + res.getDuration())){
                    okay = 0;
                    break;
                }
            }
        }
        if(okay == 1){
            reservationRepository.save(reservation);
            //Price calculator

            LocalDateTime localDateTime = reservation.getTime();
            Season season = Season.getSeasonByMonth(localDateTime.getMonth());
            DayOfWeek dayOfWeek = localDateTime.getDayOfWeek();
            DayOfTheWeek dayOfTheWeek = DayOfTheWeek.getDayOfTheWeekByDayOfWeek(dayOfWeek);


            Optional<TennisCourt> tennisCourt1 = tennisCourtRepository.findById(reservation.getTennisCourt().getId());
            Optional<SportFacility> sportFacility = sportFacilityRepository.findById(tennisCourt1.get().getSportFacility().getId());
            Optional<Rate> rate = rateRepository.findById(sportFacility.get().getRate().getId());

            Optional<SeasonModifier> seasonModifier = seasonModifierRepository.findFirstByRateAndSeason(rate.get(), season);
            Optional<HourModifier> hourModifier = hourModifierRepository.findFirstByRateAndDayOfTheWeek(rate.get(), dayOfTheWeek);

            if(seasonModifier.isPresent() && hourModifier.isPresent()){
                Float finalPrice = 0.0f;
                if(tennisCourt1.get().getNocturne()){
                    finalPrice = (sportFacility.get().getBasePrice() + rate.get().getNocturneModifier() + seasonModifier.get().getModifier() + hourModifier.get().getModifier()) * reservation.getDuration();
                }
                else{
                    finalPrice = (sportFacility.get().getBasePrice() + seasonModifier.get().getModifier() + hourModifier.get().getModifier()) * reservation.getDuration();

                }

                Document document = new Document();
                String fileLocation = null;
                try {
                    fileLocation = new File("disi\\src\\main\\resources\\static"). getAbsolutePath() + "\\bill_res.pdf";
                    PdfWriter.getInstance(document, new FileOutputStream(fileLocation));

                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                document.open();
                Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
                Chunk chunk = new Chunk("Bill for your reservation: ", font);

                try {
                    document.add(chunk);
                    document.add(new Paragraph("Base price: " + sportFacility.get().getBasePrice()));
                    document.add(new Paragraph("Nocturne Modifier: " + rate.get().getNocturneModifier()));
                    document.add(new Paragraph("Season Modifier: " + seasonModifier.get().getModifier()));
                    document.add(new Paragraph("Day of week modifier: " + hourModifier.get().getModifier()));
                    document.add(new Paragraph("Duration: " + reservation.getDuration()));
                    document.add(new Paragraph("Final price: " + finalPrice));

                } catch (DocumentException e) {
                    e.printStackTrace();
                }

                document.close();

                EmailDTO emailDTO = new EmailDTO();
                emailDTO.setName(user.getFirstName());
                emailDTO.setSubject("Bill for reservation");
                emailDTO.setTo(loggedInUser.getName());

                try {
                    emailService.sendEmail(emailDTO, fileLocation);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TemplateException e) {
                    e.printStackTrace();
                }
                return reservation.getId();
            }
            else{
                return reservation.getId();
            }

        }else{
            return (long) -1;
        }
    }

    public List<Reservation> findAll(){
        return reservationRepository.findAll();
    }

    public Long delete(Long id) {
        Reservation reservation = reservationRepository.findById(id).get();
        LocalDateTime today = LocalDateTime.now();
        Duration duration = Duration.between(reservation.getTime(), today);
        System.out.println(duration.toHours());
        if(duration.toHours() * -1 > 24) {
            reservationRepository.deleteById(id);
            return id;
        }
        return (long) -1;
    }

    public Reservation findByID(Long id) {
        return reservationRepository.findById(id).get();
    }

    public Long update(ReservationDTO reservationDTO) {
        Reservation reservation = reservationRepository.findById(reservationDTO.getId()).get();
        reservation.setTime(reservationDTO.getTime());
        reservation.setMonth(reservationDTO.getMonth());
        reservation.setDuration(reservationDTO.getDuration());
        reservationRepository.save(reservation);
        return reservation.getId();
    }

    public void acceptPlayer(Long userId, Long reservationId){
        Reservation reservation = reservationRepository.getById(reservationId);
        reservation.setSecondUserEntity(userRepository.getById(userId));
        reservation.setAccepted(true);
        reservation.setReservationTakeoverRequest(false);
        reservation.setPlayerRequest(false);

        reservationRepository.save(reservation);
    }

    public void acceptReplaceReservation(Long userId, Long reservationId){
        Reservation reservation = reservationRepository.getById(reservationId);
        reservation.setUserEntity(userRepository.getById(userId));
        reservation.setAccepted(true);
        reservation.setReservationTakeoverRequest(false);
        reservation.setPlayerRequest(false);
        reservationRepository.save(reservation);
    }

    public Long setFlagsReservation(Reservation reservation){
        reservationRepository.save(reservation);
        return reservation.getId();
    }
    
    public List<Reservation> getAllByUser(final Long userId) {
    	final UserEntity user = userRepository.getById(userId);
    	return reservationRepository.findAllByUserEntity(user);
    }
}
