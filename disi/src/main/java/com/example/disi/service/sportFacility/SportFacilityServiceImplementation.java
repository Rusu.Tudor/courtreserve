package com.example.disi.service.sportFacility;

import com.example.disi.domain.*;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.sportFacility.SportFacilityDTOInput;
import com.example.disi.dto.sportFacility.SportFacilityDTOOutput;
import com.example.disi.dto.sportFacility.SportFacilityDTOUpdate;
import com.example.disi.mapper.SportFacilityMapper;
import com.example.disi.repository.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SportFacilityServiceImplementation implements SportFacilityService {

    private final SportFacilityRepository sportFacilityRepository;
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final RateRepository rateRepository;
    private final ScheduleRepository scheduleRepository;

    public SportFacilityServiceImplementation(SportFacilityRepository sportFacilityRepository, UserRepository userRepository, AddressRepository addressRepository, RateRepository rateRepository, ScheduleRepository scheduleRepository) {
        this.sportFacilityRepository = sportFacilityRepository;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.rateRepository = rateRepository;
        this.scheduleRepository = scheduleRepository;
    }

    @Override
    public List<SportFacilityDTOOutput> getAll() {
        return SportFacilityMapper.INSTANCE.sportFacilitysToSportFacilityDTOOutputs(sportFacilityRepository.findAll());
    }

    @Override
    public SportFacilityDTOOutput getById(Long id) throws ResourceNotFoundException {
        Optional<SportFacility> sportFacility = sportFacilityRepository.findById(id);
        if(sportFacility.isPresent()){
            return SportFacilityMapper.INSTANCE.sportFacilityToSportFacilityDTOOutput(sportFacility.get());
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public SportFacilityDTOOutput getByAddress(Long addressId) throws ResourceNotFoundException {
        Optional<Address> address = addressRepository.findById(addressId);
        if(address.isPresent()){
            Optional<SportFacility> sportFacility = sportFacilityRepository.findByAddress(address.get());
            if(sportFacility.isPresent()){
                return SportFacilityMapper.INSTANCE.sportFacilityToSportFacilityDTOOutput(sportFacility.get());
            }
            else {
                throw new ResourceNotFoundException("Not found!");
            }
        }
        else {
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public SportFacilityDTOOutput getByRate(Long rateId) throws ResourceNotFoundException {
        Optional<Rate> rate = rateRepository.findById(rateId);
        if(rate.isPresent()){
            Optional<SportFacility> sportFacility = sportFacilityRepository.findByRate(rate.get());
            if(sportFacility.isPresent()){
                return SportFacilityMapper.INSTANCE.sportFacilityToSportFacilityDTOOutput(sportFacility.get());
            }
            else {
                throw new ResourceNotFoundException("Not found!");
            }
        }
        else {
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public List<SportFacilityDTOOutput> getAllByUser(Long userId) throws ResourceNotFoundException {
        Optional<UserEntity> userEntity = userRepository.findById(userId);
        if(userEntity.isPresent()){
            return SportFacilityMapper.INSTANCE.sportFacilitysToSportFacilityDTOOutputs(sportFacilityRepository.findAllByUserEntity(userEntity.get()));
        }
        else {
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public SportFacilityDTOInput create(Long rateId, SportFacilityDTOInput sportFacilityDTOInput) throws ResourceNotFoundException {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();

        UserEntity user = userRepository.findFirstByEmail(username).get();

        Optional<Rate> rate = rateRepository.findById(rateId);
        Optional<UserEntity> userEntity = userRepository.findById(user.getId());
        if(rate.isPresent() && userEntity.isPresent()){
            SportFacility sportFacility = SportFacilityMapper.INSTANCE.sportFacilityDTOInputToSportFacility(sportFacilityDTOInput);
            sportFacility.setUuid(UUID.randomUUID());
            if(!sportFacilityRepository.findByRate(rate.get()).isPresent()){
                sportFacility.setRate(rate.get());
                sportFacility.setUserEntity(userEntity.get());
                sportFacilityRepository.save(sportFacility);

                for (Season season: Season.values()) {
                    for (DayOfTheWeek dayOfTheWeek: DayOfTheWeek.values()){
                        Schedule schedule = new Schedule();
                        schedule.setOpeningTime(8);
                        schedule.setClosingTime(8);
                        schedule.setSeason(season);
                        schedule.setDayOfTheWeek(dayOfTheWeek);
                        schedule.setSportFacility(sportFacility);
                        scheduleRepository.save(schedule);
                    }
                }

                Address address = sportFacility.getAddress();
                address.setUuid(UUID.randomUUID());
                addressRepository.save(address);

                return sportFacilityDTOInput;
            }
            else{
                throw new ResourceNotFoundException("Not found!");
            }
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public SportFacilityDTOUpdate update(Long id, SportFacilityDTOUpdate sportFacilityDTOUpdate) throws ResourceNotFoundException {
        Optional<SportFacility> sportFacility = sportFacilityRepository.findById(id);
        if(sportFacility.isPresent()){
            SportFacility sportFacility1 = SportFacilityMapper.INSTANCE.sportFacilityDTOUpdateToSportFacility(sportFacilityDTOUpdate);
            sportFacility1.setId(sportFacility.get().getId());
            sportFacility1.setUuid(sportFacility.get().getUuid());
            sportFacility1.setRate(sportFacility.get().getRate());
            sportFacility1.setUserEntity(sportFacility.get().getUserEntity());
            sportFacility1.setAddress(sportFacility.get().getAddress());
            sportFacilityRepository.save(sportFacility1);
            return sportFacilityDTOUpdate;
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException {
        Optional<SportFacility> sportFacility = sportFacilityRepository.findById(id);
        if(sportFacility.isPresent()){
            sportFacilityRepository.deleteById(sportFacility.get().getId());
        }
        else {
            throw new ResourceNotFoundException("Not found!");
        }
    }
}
