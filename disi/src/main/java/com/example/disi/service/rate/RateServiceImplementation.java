package com.example.disi.service.rate;

import com.example.disi.domain.HourModifier;
import com.example.disi.domain.Rate;
import com.example.disi.domain.SeasonModifier;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.rate.RateDTOInput;
import com.example.disi.dto.rate.RateDTO;
import com.example.disi.mapper.RateMapper;
import com.example.disi.repository.HourModifierRepository;
import com.example.disi.repository.RateRepository;
import com.example.disi.repository.SeasonModifierRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RateServiceImplementation implements RateService{

    private final RateRepository rateReposiory;
    private final HourModifierRepository hourModifierRepository;
    private final SeasonModifierRepository seasonModifierRepository;

    public RateServiceImplementation(RateRepository rateReposiory, HourModifierRepository hourModifierRepository, SeasonModifierRepository seasonModifierRepository) {
        this.rateReposiory = rateReposiory;
        this.hourModifierRepository = hourModifierRepository;
        this.seasonModifierRepository = seasonModifierRepository;
    }

    @Override
    public List<RateDTO> getAll() {
        return RateMapper.INSTANCE.ratesToRateDTOOutputs(rateReposiory.findAll());
    }

    @Override
    public RateDTO getById(Long id) throws ResourceNotFoundException {
        Optional<Rate> rate = rateReposiory.findById(id);
        if(rate.isPresent()){
            return RateMapper.INSTANCE.toDTO(rate.get());
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public RateDTOInput create(RateDTOInput rateDTOInput) {
        Rate rate = RateMapper.INSTANCE.rateDTOInputToRate(rateDTOInput);
        rate.setUuid(UUID.randomUUID());
        rate = rateReposiory.save(rate);

        for (DayOfTheWeek dayOfTheWeek: DayOfTheWeek.values()) {
            if(dayOfTheWeek.equals(DayOfTheWeek.SUNDAY) || dayOfTheWeek.equals(DayOfTheWeek.SATURDAY)){
                HourModifier hourModifier = new HourModifier();
                hourModifier.setModifier(rateDTOInput.getWeekendModifier());
                hourModifier.setRate(rate);
                hourModifier.setDayOfTheWeek(dayOfTheWeek);
                hourModifierRepository.save(hourModifier);
            }
            else{
                HourModifier hourModifier = new HourModifier();
                hourModifier.setModifier(rateDTOInput.getWorkingDaysModifier());
                hourModifier.setRate(rate);
                hourModifier.setDayOfTheWeek(dayOfTheWeek);
                hourModifierRepository.save(hourModifier);
            }
        }

        SeasonModifier seasonModifier_summer = new SeasonModifier();
        seasonModifier_summer.setModifier(rateDTOInput.getSummerModifier());
        seasonModifier_summer.setSeason(Season.SUMMER);
        seasonModifier_summer.setRate(rate);
        seasonModifierRepository.save(seasonModifier_summer);

        SeasonModifier seasonModifier_spring = new SeasonModifier();
        seasonModifier_spring.setModifier(rateDTOInput.getSpringModifier());
        seasonModifier_spring.setSeason(Season.SPRING);
        seasonModifier_spring.setRate(rate);
        seasonModifierRepository.save(seasonModifier_spring);

        SeasonModifier seasonModifier_autumn = new SeasonModifier();
        seasonModifier_autumn.setModifier(rateDTOInput.getAutumnModifier());
        seasonModifier_autumn.setSeason(Season.AUTUMN);
        seasonModifier_autumn.setRate(rate);
        seasonModifierRepository.save(seasonModifier_autumn);

        SeasonModifier seasonModifier_winter = new SeasonModifier();
        seasonModifier_winter.setModifier(rateDTOInput.getWinterModifier());
        seasonModifier_winter.setSeason(Season.WINTER);
        seasonModifier_winter.setRate(rate);
        seasonModifierRepository.save(seasonModifier_winter);

        return RateMapper.INSTANCE.rateToRateDTOInput(rate);
    }

    @Override
    public RateDTOInput update(Long id, RateDTOInput rateDTOInput) throws ResourceNotFoundException {
        Optional<Rate> rate = rateReposiory.findById(id);
        if(rate.isPresent()){
            Rate rate1 = RateMapper.INSTANCE.rateDTOInputToRate(rateDTOInput);
            rate1.setId(rate.get().getId());
            rate1.setUuid(rate.get().getUuid());
            rateReposiory.save(rate1);
            return rateDTOInput;
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException {
        Optional<Rate> rate = rateReposiory.findById(id);
        if(rate.isPresent()){
            rateReposiory.deleteById(rate.get().getId());
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }
}
