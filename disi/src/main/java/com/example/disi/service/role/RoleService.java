package com.example.disi.service.role;

import com.example.disi.domain.RoleEntity;
import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleService.class);
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public RoleEntity getRoleByName(String name) {
        RoleEntity roleEntity = roleRepository.findFirstByName(name);
        if(roleEntity == null){
            LOGGER.error("Role "+name+" was not found");
            throw new ResourceNotFoundException("Role not found");
        }
        return roleRepository.findFirstByName(name);
    }
}
