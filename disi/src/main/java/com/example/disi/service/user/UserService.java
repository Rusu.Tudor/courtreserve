package com.example.disi.service.user;

import com.example.disi.domain.RoleEntity;
import com.example.disi.dto.user.UserAccountDTO;
import com.example.disi.dto.user.UserEntityDTO;
import com.example.disi.dto.user.UserInsertDTO;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.UUID;

public interface UserService {

    public UserDetails loadUserByUsername(String s);
    public Long insert(UserInsertDTO userInsertDTO, RoleEntity roleEntity);

    List<UserAccountDTO> getAllUsers();

    UserAccountDTO getUserById(Long id);

    UserEntityDTO updateUser(Long id, UserEntityDTO userAccountDTO);

    void deleteUser(Long id);
}
