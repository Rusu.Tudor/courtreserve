package com.example.disi.service.subscription;

import com.example.disi.domain.*;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import com.example.disi.dto.email.EmailDTO;
import com.example.disi.dto.subscription.SubscriptionDTO;
import com.example.disi.mapper.SubscriptionMapper;
import com.example.disi.repository.*;
import com.example.disi.service.email.EmailService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import freemarker.template.TemplateException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class SubscriptionServiceImplementation implements SubscriptionService {
    private final SubscriptionRepository subscriptionRepository;
    private final TennisCourtRepository tennisCourtRepository;
    private final UserRepository userRepository;
    private final SportFacilityRepository sportFacilityRepository;
    private final RateRepository rateRepository;
    private final SeasonModifierRepository seasonModifierRepository;
    private final HourModifierRepository hourModifierRepository;
    private final EmailService emailService;

    public SubscriptionServiceImplementation(SubscriptionRepository subscriptionRepository, TennisCourtRepository tennisCourtRepository, UserRepository userRepository, SportFacilityRepository sportFacilityRepository, RateRepository rateRepository, SeasonModifierRepository seasonModifierRepository, HourModifierRepository hourModifierRepository, EmailService emailService) {
        this.subscriptionRepository = subscriptionRepository;
        this.tennisCourtRepository = tennisCourtRepository;
        this.userRepository = userRepository;
        this.sportFacilityRepository = sportFacilityRepository;
        this.rateRepository = rateRepository;
        this.seasonModifierRepository = seasonModifierRepository;
        this.hourModifierRepository = hourModifierRepository;
        this.emailService = emailService;
    }

    public Long insert(SubscriptionDTO subscriptionDTO, Long id) {
        Subscription subscription = SubscriptionMapper.INSTANCE.subscriptionDTOToSubscription(subscriptionDTO);

        TennisCourt tennisCourt = tennisCourtRepository.getById(id);
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();

        UserEntity user = userRepository.findFirstByEmail(username).get();

        subscription.setTennisCourt(tennisCourt);
        subscription.setUserEntity(user);
        subscriptionRepository.save(subscription);

        //Price calculator

        LocalDateTime localDateTime = subscription.getTime();
        Season season = Season.getSeasonByMonth(localDateTime.getMonth());
        DayOfWeek dayOfWeek = localDateTime.getDayOfWeek();
        DayOfTheWeek dayOfTheWeek = DayOfTheWeek.getDayOfTheWeekByDayOfWeek(dayOfWeek);


        Optional<TennisCourt> tennisCourt1 = tennisCourtRepository.findById(subscription.getTennisCourt().getId());
        Optional<SportFacility> sportFacility = sportFacilityRepository.findById(tennisCourt1.get().getSportFacility().getId());
        Optional<Rate> rate = rateRepository.findById(sportFacility.get().getRate().getId());

        Optional<SeasonModifier> seasonModifier = seasonModifierRepository.findFirstByRateAndSeason(rate.get(), season);
        Optional<HourModifier> hourModifier = hourModifierRepository.findFirstByRateAndDayOfTheWeek(rate.get(), dayOfTheWeek);

        if(seasonModifier.isPresent() && hourModifier.isPresent()){
            Float finalPrice = 0.0f;
            if(tennisCourt1.get().getNocturne()){
                finalPrice = (sportFacility.get().getBasePrice() + rate.get().getNocturneModifier() + seasonModifier.get().getModifier() + hourModifier.get().getModifier()) * subscription.getDuration() * 25.0f;
            }
            else{
                finalPrice = (sportFacility.get().getBasePrice() + seasonModifier.get().getModifier() + hourModifier.get().getModifier()) * subscription.getDuration() * 25.0f;

            }

            Document document = new Document();
            String fileLocation = null;

            try {
                fileLocation = new File("disi\\src\\main\\resources\\static"). getAbsolutePath() + "\\bill_sub.pdf";
                PdfWriter.getInstance(document, new FileOutputStream(fileLocation));

            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            Chunk chunk = new Chunk("Bill for your subscription: ", font);

            try {
                document.add(chunk);
                document.add(new Paragraph("Base price: " + sportFacility.get().getBasePrice()));
                document.add(new Paragraph("Nocturne Modifier: " + rate.get().getNocturneModifier()));
                document.add(new Paragraph("Season Modifier: " + seasonModifier.get().getModifier()));
                document.add(new Paragraph("Day of week modifier: " + hourModifier.get().getModifier()));
                document.add(new Paragraph("Duration: " + subscription.getDuration()));
                document.add(new Paragraph("Final price: " + finalPrice));

            } catch (DocumentException e) {
                e.printStackTrace();
            }

            document.close();

            EmailDTO emailDTO = new EmailDTO();
            emailDTO.setName(user.getFirstName());
            emailDTO.setSubject("Bill for subscription");
            emailDTO.setTo(loggedInUser.getName());

            try {
                emailService.sendEmail(emailDTO, fileLocation);
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TemplateException e) {
                e.printStackTrace();
            }
            return subscription.getId();
        }
        else{
            return subscription.getId();
        }
    }

    public List<Subscription> findAll() {
        return subscriptionRepository.findAll();
    }

    public void delete(Long id) {
        subscriptionRepository.deleteById(id);
    }

    public Subscription findByID(Long id) {
        return subscriptionRepository.findById(id).get();
    }

    public Long update(SubscriptionDTO subscriptionDTO) {
        Subscription subscription = subscriptionRepository.findById(subscriptionDTO.getId()).get();
        subscription.setTime(subscriptionDTO.getTime());
        subscription.setDuration(subscriptionDTO.getDuration());
        subscription.setMonth(subscriptionDTO.getMonth());
        subscriptionRepository.save(subscription);
        return subscription.getId();
    }
}
