package com.example.disi.service.email;

import com.example.disi.dto.email.EmailDTO;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfReader;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {

    private final JavaMailSender mailSender;
    private final Configuration configuration;

    public EmailService(JavaMailSender mailSender, Configuration configuration) {
        this.mailSender = mailSender;
        this.configuration = configuration;
    }

    public void sendEmail(EmailDTO emailDTO, String fileLocation) throws MessagingException, IOException, TemplateException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        //nevoie de MULTIPART MIXED pentru atasamente
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

        FileSystemResource fileSystemResource = new FileSystemResource(fileLocation);
        helper.addAttachment(fileSystemResource.getFilename(), fileSystemResource);

        helper.setSubject(emailDTO.getSubject());
        helper.setTo(emailDTO.getTo());


        StringWriter stringWriter = new StringWriter();
        Map<String, Object> model = new HashMap<>();
        model.put("email", emailDTO);
        configuration.getTemplate("email.ftl").process(model, stringWriter);

        helper.setText(stringWriter.getBuffer().toString(), true);
        mailSender.send(mimeMessage);
    }

}
