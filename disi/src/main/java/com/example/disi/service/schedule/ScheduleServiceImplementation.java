package com.example.disi.service.schedule;

import com.example.disi.domain.Schedule;
import com.example.disi.domain.SportFacility;
import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOOutput;
import com.example.disi.mapper.ScheduleMapper;
import com.example.disi.repository.ScheduleRepository;
import com.example.disi.repository.SportFacilityRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ScheduleServiceImplementation implements ScheduleService{


    private final ScheduleRepository scheduleRepository;
    private final SportFacilityRepository sportFacilityRepository;

    public ScheduleServiceImplementation(ScheduleRepository scheduleRepository, SportFacilityRepository sportFacilityRepository) {
        this.scheduleRepository = scheduleRepository;
        this.sportFacilityRepository = sportFacilityRepository;
    }

    @Override
    public List<ScheduleDTOOutput> getAll() {
        return ScheduleMapper.INSTANCE.schedulesToScheduleDTOOutputs(scheduleRepository.findAll());
    }

    @Override
    public ScheduleDTOOutput getById(Long id) throws ResourceNotFoundException {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if(schedule.isPresent()){
            return ScheduleMapper.INSTANCE.scheduleToScheduleDTOOutput(schedule.get());
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public List<ScheduleDTOOutput> getAllBySportFacility(Long sportFacilityId) throws ResourceNotFoundException {
        Optional<SportFacility> sportFacility = sportFacilityRepository.findById(sportFacilityId);
        if(sportFacility.isPresent()){
            return ScheduleMapper.INSTANCE.schedulesToScheduleDTOOutputs(scheduleRepository.findAllBySportFacility(sportFacility.get()));
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public ScheduleDTOInput create(Long sportFacilityId, ScheduleDTOInput scheduleDTOInput) throws ResourceNotFoundException {
        Optional<SportFacility> sportFacility = sportFacilityRepository.findById(sportFacilityId);
        if(sportFacility.isPresent()){
            Schedule schedule = ScheduleMapper.INSTANCE.scheduleDTOInputToSchedule(scheduleDTOInput);
            schedule.setSportFacility(sportFacility.get());
            schedule.setUuid(UUID.randomUUID());
            scheduleRepository.save(schedule);
            return scheduleDTOInput;
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public ScheduleDTOInput update(Long id, ScheduleDTOInput scheduleDTOInput) throws ResourceNotFoundException {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if(schedule.isPresent()){
            Schedule schedule1 = ScheduleMapper.INSTANCE.scheduleDTOInputToSchedule(scheduleDTOInput);
            schedule1.setId(schedule.get().getId());
            schedule1.setUuid(schedule.get().getUuid());
            schedule1.setSportFacility(schedule.get().getSportFacility());
            scheduleRepository.save(schedule1);
            return scheduleDTOInput;
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if(schedule.isPresent()){
            scheduleRepository.deleteById(schedule.get().getId());
        }
        else{
            throw new ResourceNotFoundException("Not found!");
        }
    }
}
