package com.example.disi.service.tennisCourt;

import com.example.disi.domain.Reservation;
import com.example.disi.domain.SportFacility;
import com.example.disi.domain.Subscription;
import com.example.disi.domain.TennisCourt;
import com.example.disi.dto.reservation.AddReservationDTO;
import com.example.disi.dto.subscription.AddSubscriptionDTO;
import com.example.disi.dto.tennisCourt.TennisCourtDTO;
import com.example.disi.mapper.TennisCourtMapper;
import com.example.disi.repository.ReservationRepository;
import com.example.disi.repository.SportFacilityRepository;
import com.example.disi.repository.SubscriptionRepository;
import com.example.disi.repository.TennisCourtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TennisCourtServiceImplementation implements TennisCourtService {
    private final TennisCourtRepository tennisCourtRepository;
    private final SportFacilityRepository sportFacilityRepository;

    public TennisCourtServiceImplementation(TennisCourtRepository tennisCourtRepository, SportFacilityRepository sportFacilityRepository) {
        this.tennisCourtRepository = tennisCourtRepository;
        this.sportFacilityRepository = sportFacilityRepository;
    }

    public Long insert(TennisCourtDTO tennisCourtDTO, Long sportFacilityId) {
        TennisCourt tennisCourt = TennisCourtMapper.INSTANCE.tennisCourtDTOToTennisCourt(tennisCourtDTO);
        tennisCourt = tennisCourtRepository.save(tennisCourt);
        SportFacility sportFacility = sportFacilityRepository.getById(sportFacilityId);
        List<TennisCourt> tennisCourts = sportFacility.getTennisCourts();
        tennisCourt.setSportFacility(sportFacility);
        tennisCourts.add(tennisCourt);
        sportFacility.setTennisCourts(tennisCourts);
        sportFacilityRepository.save(sportFacility);
        return tennisCourt.getId();
    }

    public List<TennisCourtDTO> findAll() {
        return TennisCourtMapper.INSTANCE.tennisCourtsToTennisCourtsDTO(tennisCourtRepository.findAll());
    }
    
    public void delete(final Long id) {
    	TennisCourt t = tennisCourtRepository.getById(id);
    	t.getSportFacility().getTennisCourts().remove(t);
    	t.setSportFacility(null);
    	tennisCourtRepository.deleteById(id);

    }
    
    public List<TennisCourtDTO> getAllBySportFacility(final Long sportFacilityId) {
    	final SportFacility sportFacility = sportFacilityRepository.getById(sportFacilityId);
    	final List<TennisCourt> tennisCourts = tennisCourtRepository.findAllBySportFacility(sportFacility);
    	return TennisCourtMapper.INSTANCE.tennisCourtsToTennisCourtsDTO(tennisCourts);
    }
}
