package com.example.disi.service.rate;

import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.rate.RateDTOInput;
import com.example.disi.dto.rate.RateDTO;

import java.util.List;

public interface RateService {

    List<RateDTO> getAll();
    RateDTO getById(Long id) throws ResourceNotFoundException;
    RateDTOInput create(RateDTOInput rateDTOInput);
    RateDTOInput update(Long id, RateDTOInput rateDTOInput) throws ResourceNotFoundException;
    void delete(Long id) throws ResourceNotFoundException;
}
