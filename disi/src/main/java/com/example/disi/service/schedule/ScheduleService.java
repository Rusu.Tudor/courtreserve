package com.example.disi.service.schedule;

import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOOutput;

import java.util.List;

public interface ScheduleService {

    List<ScheduleDTOOutput> getAll();
    ScheduleDTOOutput getById(Long id) throws ResourceNotFoundException;
    List<ScheduleDTOOutput> getAllBySportFacility(Long sportFacilityId) throws ResourceNotFoundException;
    ScheduleDTOInput create(Long sportFacilityId, ScheduleDTOInput scheduleDTOInput) throws ResourceNotFoundException ;
    ScheduleDTOInput update(Long id, ScheduleDTOInput scheduleDTOInput) throws ResourceNotFoundException;
    void delete(Long id) throws ResourceNotFoundException;
}
