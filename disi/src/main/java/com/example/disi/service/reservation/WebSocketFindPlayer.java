package com.example.disi.service.reservation;

import com.example.disi.dto.reservation.ReservationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.messaging.simp.SimpMessagingTemplate;
@Service
public class WebSocketFindPlayer {

    @Autowired
    private SimpMessagingTemplate simpleMessageTemplate;


    public void sendNotificationToPlayers(String message) {
        this.simpleMessageTemplate.convertAndSend("/findPlayer", message);
    }

    public void sendNotificationToReplaceReservation(String message) {
        this.simpleMessageTemplate.convertAndSend("/replaceReservation", message);
    }
}
