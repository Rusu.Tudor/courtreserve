package com.example.disi.service.sportFacility;

import com.example.disi.domain.commons.exceptions.ResourceNotFoundException;
import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOOutput;
import com.example.disi.dto.sportFacility.SportFacilityDTOInput;
import com.example.disi.dto.sportFacility.SportFacilityDTOOutput;
import com.example.disi.dto.sportFacility.SportFacilityDTOUpdate;

import java.util.List;

public interface SportFacilityService {

    List<SportFacilityDTOOutput> getAll();
    SportFacilityDTOOutput getById(Long id) throws ResourceNotFoundException;
    SportFacilityDTOOutput getByAddress(Long addressId) throws ResourceNotFoundException;
    SportFacilityDTOOutput getByRate(Long rateId) throws ResourceNotFoundException;
    List<SportFacilityDTOOutput> getAllByUser(Long userId) throws ResourceNotFoundException;
    SportFacilityDTOInput create(Long rateId, SportFacilityDTOInput sportFacilityDTOInput) throws ResourceNotFoundException ;
    SportFacilityDTOUpdate update(Long id, SportFacilityDTOUpdate sportFacilityDTOUpdate) throws ResourceNotFoundException;
    void delete(Long id) throws ResourceNotFoundException;
}
