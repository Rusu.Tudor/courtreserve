package com.example.disi.dto.rate;

import com.example.disi.dto.HourModifierDTO;
import com.example.disi.dto.SeasonModifierDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateDTO {

    private Long id;
    private Float nocturneModifier;
    private List<SeasonModifierDTO> seasonModifiers;
    private List<HourModifierDTO> hourModifiers;

}