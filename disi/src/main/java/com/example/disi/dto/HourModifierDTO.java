package com.example.disi.dto;

import com.example.disi.domain.commons.enums.DayOfTheWeek;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HourModifierDTO {

    private Float modifier;
    private DayOfTheWeek dayOfTheWeek;
}
