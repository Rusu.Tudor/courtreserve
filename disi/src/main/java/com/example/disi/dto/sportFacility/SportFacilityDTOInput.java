package com.example.disi.dto.sportFacility;

import com.example.disi.domain.UserEntity;
import com.example.disi.dto.address.AddressDTO;
import com.example.disi.dto.rate.RateDTOInput;
import com.example.disi.dto.schedule.ScheduleDTOInput;
import com.example.disi.dto.tennisCourt.TennisCourtDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SportFacilityDTOInput {

    private Float basePrice;
    private AddressDTO address;
}
