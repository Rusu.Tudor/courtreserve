package com.example.disi.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddReservationDTO {
    private Long reservationID;
    private Long tennisCourtID;
}
