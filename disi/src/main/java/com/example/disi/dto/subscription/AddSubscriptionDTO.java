package com.example.disi.dto.subscription;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddSubscriptionDTO {
    private Long subscriptionID;
    private Long tennisCourtID;
}
