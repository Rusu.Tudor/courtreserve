package com.example.disi.dto.sportFacility;

import com.example.disi.dto.address.AddressDTO;
import com.example.disi.dto.rate.RateDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SportFacilityDTOOutput {

    private Long id;
    private Float basePrice;
    private AddressDTO address;
    private RateDTO rate;
}
