package com.example.disi.dto.schedule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleTimeDTO {
    private Integer openingTime;
    private Integer closingTime;
}
