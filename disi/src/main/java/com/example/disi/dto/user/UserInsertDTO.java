package com.example.disi.dto.user;

import com.example.disi.dto.address.AddressDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInsertDTO {

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private AddressDTO address;
}
