package com.example.disi.dto.rate;

import com.example.disi.domain.HourModifier;
import com.example.disi.domain.SeasonModifier;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateDTOInput {

    private Long id;
    private Float nocturneModifier;
    private Float springModifier;
    private Float winterModifier;
    private Float summerModifier;
    private Float autumnModifier;
    private Float workingDaysModifier;
    private Float weekendModifier;
}
