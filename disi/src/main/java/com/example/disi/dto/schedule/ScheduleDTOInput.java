package com.example.disi.dto.schedule;

import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDTOInput {

    private Integer openingTime;
    private Integer closingTime;
    private Season season;
    private DayOfTheWeek dayOfTheWeek;
}