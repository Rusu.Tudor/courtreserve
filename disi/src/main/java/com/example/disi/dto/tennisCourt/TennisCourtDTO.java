package com.example.disi.dto.tennisCourt;

import com.example.disi.dto.reservation.ReservationDTO;
import com.example.disi.dto.subscription.SubscriptionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TennisCourtDTO {
    private Long id;
    private String type;
    private Boolean nocturne;
    private List<ReservationDTO> reservations;
    private List<SubscriptionDTO> subscriptions;
}
