package com.example.disi.dto.sportFacility;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SportFacilityDTOUpdate {

    private Float basePrice;
}
