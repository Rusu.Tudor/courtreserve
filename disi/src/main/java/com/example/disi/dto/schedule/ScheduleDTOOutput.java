package com.example.disi.dto.schedule;

import com.example.disi.domain.SportFacility;
import com.example.disi.domain.commons.enums.DayOfTheWeek;
import com.example.disi.domain.commons.enums.Season;
import com.example.disi.dto.sportFacility.SportFacilityDTOOutput;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDTOOutput {

    private Long id;
    private Integer openingTime;
    private Integer closingTime;
    private Season season;
    private DayOfTheWeek dayOfTheWeek;
    private SportFacilityDTOOutput sportFacility;
}
