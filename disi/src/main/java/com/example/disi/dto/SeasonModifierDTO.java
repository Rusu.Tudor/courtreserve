package com.example.disi.dto;

import com.example.disi.domain.commons.enums.Season;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeasonModifierDTO {

    private Float modifier;
    private Season season;
}
