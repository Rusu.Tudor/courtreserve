package com.example.disi.dto.jwt;

public class JWTDao {

    private Long id;
    private String roleName;
    private String username;
    private String jwtString;

    public JWTDao(){

    }

    public JWTDao(Long id, String roleName, String username, String jwtString) {
        this.id = id;
        this.roleName = roleName;
        this.username = username;
        this.jwtString = jwtString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJwtString() {
        return jwtString;
    }

    public void setJwtString(String jwtString) {
        this.jwtString = jwtString;
    }
}

